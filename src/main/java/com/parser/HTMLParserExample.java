package com.parser;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class HTMLParserExample {

	public static void main(String[] args) {

		Document doc;
		try {
			// need http protocol
			doc = Jsoup
					.connect(
							"http://mysupermarket.org.ua/index.php?myr_shop=billa")
					.userAgent(
							"Mozilla/5.0 (X11; Linux x86_64) "
									+ "AppleWebKit/537.36 (KHTML, like Gecko) "
									+ "Chrome/36.0.1985.125 Safari/537.36")
					.get();

			// get page title
			String title = doc.title();
			System.out.println("title : " + title + "\n");

			// get p with list of categories
			Element p = doc.select(
					"table > tbody > tr > td > table > tbody > tr > td > p")
					.get(3);

			// get a tags of all categories and print to console
			Elements links = p.getElementsByTag("a");
			for (Element each : links) {
				// Split url by "=". Get last element and take number from it.
				String[] stringArray = each.absUrl("href").split("=");
				Integer categoryId = Integer
						.parseInt(stringArray[stringArray.length - 1]);

				System.out.println(each.absUrl("href") + " - " + each.text()
						+ " - ID категории:" + categoryId);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
